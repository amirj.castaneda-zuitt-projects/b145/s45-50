import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { Fragment } from 'react';

export default function AppNavBar() {

	const [user, setUser] = useState(localStorage.getItem('email'))

	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand as={Link} to='/'>Zuitt</Navbar.Brand>
			<Navbar.Toggle/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to='/'>Home</Nav.Link>
					<Nav.Link as={Link} to='/courses'> Courses</Nav.Link>
					{
						(user !== null) ? 
						<Nav.Link as={Link} to='/logout'>Logout</Nav.Link>
						: 
						<Fragment>
							<Nav.Link as={Link} to='/login'> Login</Nav.Link>
							<Nav.Link as={Link} to='/register'> Register</Nav.Link>
						</Fragment>
					}
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}
