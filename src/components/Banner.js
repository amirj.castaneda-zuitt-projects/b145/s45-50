import { Button, Row, Col, Nav } from 'react-bootstrap';
import { useHistory ,useLocation, Link } from 'react-router-dom';
import { Fragment } from 'react';



export default function Banner() {
	const location = useLocation()
	const pathname = location.pathname;
	console.log(pathname);
	return (
		<Fragment>{
		(pathname === '/') ?
		<Row>
			<Col className="p-5">
				<h1>Zuitt Coding Bootcamp</h1>
				<p>Opportunities for everyone, everywhere</p>
				<Button variant="primary">Enroll Now</Button>
			</Col>
		</Row>
		:
		<Fragment>
		<Row>
			<Col className="">
				<h1>Page Not Found</h1>
				<p>Go back to <a href='/'>homepage</a></p>
			</Col>
		</Row>
		</Fragment>
	}
		</Fragment>
	)
}