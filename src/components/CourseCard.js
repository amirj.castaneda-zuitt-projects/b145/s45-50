import {useState, useEffect} from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {
	const {name, description, price} = courseProp;

	// Syntax
		// const [getter, setter] = useState(initialValueofGetter)
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);
	function enroll() {
		if(seat > 0) {
			setCount(count + 1);
			setSeat(seat - 1);
		}
	}

	useEffect(() => {
		if(seat === 0) {
			alert('No more seats available')
		}
	}, [seat])

	return(
		<Row className="my-2">
			<Col>
				<Card className="cardHighlight mt-3">
					<Card.Body>
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>												
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<Card.Text>Seats: {seat}</Card.Text>
						<Card.Text>Enrollees: {count}</Card.Text>
						<Button variant="primary" onClick={enroll}>Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>		
	)
}