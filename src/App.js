import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router} from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import './App.css';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';

function App() {
  return (
    <Router>
      <AppNavbar/>
      <Container>
      <Routes>
        <Route exact path = "/" element={<Home/>}/>
        <Route exact path = "/courses" element={<Courses/>}/>
        <Route exact path = "/register" element={<Register/>}/>
        <Route exact path = "/login" element={<Login/>}/>
        <Route exact path = "/logout" element={<Logout/>}/>
        <Route exact path = "/:incorrectUrl" element={<Error/>}/>
      </Routes>
      </Container>
    </Router>
  );
}

export default App;
