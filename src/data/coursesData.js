const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro accusamus sint optio, earum voluptate unde ducimus velit doloribus pariatur nostrum quae laborum ab molestias laboriosam eos atque quis molestiae distinctio?",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro accusamus sint optio, earum voluptate unde ducimus velit doloribus pariatur nostrum quae laborum ab molestias laboriosam eos atque quis molestiae distinctio?",
		price: 50000,
		onOffer: true
	},	
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro accusamus sint optio, earum voluptate unde ducimus velit doloribus pariatur nostrum quae laborum ab molestias laboriosam eos atque quis molestiae distinctio?",
		price: 55000,
		onOffer: true
	}
];

export default coursesData;