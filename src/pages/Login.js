import {Fragment, useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setActive] = useState(false);

	function authenticate(e) {
		e.preventDefault();

		localStorage.setItem('email', email);
		// localStorage.setItem('key', value)

		setEmail('');
		setPassword('');
		alert('You are now logged in!')
	}

	useEffect(() => {
		if(email !== '' && password !== '') {
			setActive(true);
		}
		else {
			setActive(false);
		}
	}, [email, password, isActive])

	return (
		<Fragment>
		<h1 className="my-3">Login</h1>
		<Form onSubmit={(e) => authenticate(e)}>

			<Form.Group controlId="loginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="Email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant="success" type="submit" id="loginBtn" className="my-3">
					Submit
				</Button>
			 :
			 	<Button variant="success" type="submit" id="loginBtn" className="my-3" disabled>
					Submit
				</Button>
			}
		</Form>
		</Fragment>

	)
}